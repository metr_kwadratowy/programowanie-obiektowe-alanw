#Zad 1

def fun(x):
    if x > 17:
        print(x - 17)
    elif x < 17:
        print(abs((x - 17) * 2))

x = int(input("Podaj liczbę: "))
fun(x)

#Zad 2
def funk(x):
    if x < 1 or x >2000000:
        print('a')
    elif x == 1:
        print('Ostatnia liczba silni:', 1)
    elif x == 2:
        print('Ostatnia liczba silni:', 2)
    elif x == 3:
        print('Ostatnia liczba silni:', 3)
    elif x == 4:
        print('Ostatnia liczba silni:', 4)
    else:
        print(0)
        
a = int(input('Podaj liczbę: '))
funk(a)

#Zad 3

x = int(input('podaj x: '))
print(x//5 + x//25)